let tasks_got = 0;
let time_begin;


request_tasks.onclick = () => {
    $.get(`/api/get-tasks?reference=default`, (res) => {
        btn_check_tasks.hidden = false;
        task_area.innerHTML = '';
        score_area.innerHTML = '';
        tasks_got = res.length;

        res.forEach((task) => {
            task_area.innerHTML += `
                <div>
                    <strong>${task.task}</strong>
                    <pre class="text-white">${task.description}</pre>
                    Ответ: <input data-task-id="${task.id}" class="task_answer">
                </div>
                <hr>
            `;
        });
    });

    time_begin = Date.now();
};


btn_check_tasks.onclick = () => {
    const answers = [];

    for (answer_input of $('.task_answer')) {
        const answer = answer_input.value;
        const task_id = answer_input.dataset.taskId;

        if (!answer) {
            alert('Нужно все заполнить');
            return;
        }

        answers.push({
            id: task_id,
            answer,
        });
    }

    $.post('/api/check-tasks', {
        answers: JSON.stringify(answers),
        author: $('#author').val() || undefined,
        time_took: (Date.now() - time_begin) / 1000,
    }, (res) => {
        btn_check_tasks.hidden = true;
        const user_answers = $('.task_answer');

        for (let i = 0; i < tasks_got; i++) {
            const answer_input = user_answers[i];
            const task_id = answer_input.dataset.taskId;
            const task_answer = res[task_id];
            answer_input.readOnly = true;

            if (task_answer === undefined) {
                answer_input.style.backgroundColor = 'green';
                continue;
            }

            answer_input.style.background = 'red';
            answer_input.title = `Правильно: ${task_answer}`;
        }

        const accepted_tasks_count = tasks_got - Object.keys(res).length;
        score_area.innerHTML += `
            <br>
            <div>
                Ваш результат: ${accepted_tasks_count} из ${tasks_got}
                (${(accepted_tasks_count / tasks_got * 100).toFixed(2)} %)
            </div>
        `;
    });
};
