from flask import Blueprint, render_template, abort
from sqlalchemy import desc
from jinja2.exceptions import TemplateNotFound

from app.utils import localize_fromutc
from app.database.core import DBConnection
from app.database.models import TestResult


views = Blueprint('views', __name__)


@views.route('/')
def route_index():
    return render_template('index.html')


@views.route('/articles/<template>')
def route_template(template):
    try:
        return render_template(f'articles/{template}.html')
    except TemplateNotFound:
        abort(404)


@views.route('/test')
def route_test():
    return render_template('test.html')


@views.route('/admin')
def route_admin():
    return render_template('admin.html')


@views.route('/top')
def route_top():
    db = DBConnection()
    top = db.query(TestResult).order_by(desc(TestResult.percent), TestResult.time_took).limit(10).all()
    for result in top:
        result.time_finish = localize_fromutc(result.time_finish)
    return render_template('top.html', top=top)
