from app.database.core import engine
from app.database.models import Base


def create_database():
    Base.metadata.create_all(engine)
