from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker

from app.config import DATABASE_FILE


engine = create_engine(f'sqlite:///{DATABASE_FILE}')
DBConnection = sessionmaker(bind=engine)
