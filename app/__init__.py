from logging import basicConfig, FileHandler, INFO

from .main import app
from .config import LOGFILE, LOGGING_FORMAT, DATEFORMAT


basicConfig(
    level=INFO,
    handlers=[FileHandler(LOGFILE, 'a', 'utf-8')],
    format=LOGGING_FORMAT,
    datefmt=DATEFORMAT,
)
